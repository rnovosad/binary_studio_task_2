import Fighter from '../models/fighter'

class Battle {
  constructor(fighter1, fighter2) {
    this.fighter1 = new Fighter(fighter1)
    this.fighter2 = new Fighter(fighter2)
  }

  fight() {
    while (this.fighter1.health > 0 && this.fighter2.health > 0) {
      let firstTurn = this.fighter1.getHitPower() - this.fighter2.getBlockPower()
      let secondTurn = this.fighter2.getHitPower() - this.fighter1.getBlockPower()
      if (firstTurn > 0 && this.fighter2.health > 0) {
        this.fighter2.health -= firstTurn
      }
      if (secondTurn > 0 && this.fighter1.health) {
        this.fighter1.health -= secondTurn
      }
    }
    if (this.fighter1.health <= 0) {
      return this.fighter2.name
    }
    if (this.fighter2.health <= 0) {
      return this.fighter1.name
    }
  }
} 

export default Battle
