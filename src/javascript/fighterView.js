import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();
    this.handleCheck = this.handleCheck.bind(this)
    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const checkbox = this.createCheckbox(name);

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(nameElement, imageElement, checkbox);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  }

  handleCheck() {
    const check = document.getE('input')
    this.isChecked = !this.isChecked
  }

  createCheckbox(name) {
    return this.createElement({
      tagName: 'input',
      className: 'name',
      attributes: {
        type: 'checkbox',
        id: `cbx-${name}`
      }
    })
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    return this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });
  }
}

export default FighterView;
