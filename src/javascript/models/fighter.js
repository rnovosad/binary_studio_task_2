class Fighter {
  constructor(fighter) {
    this.name = fighter.name
    this.attack = fighter.attack
    this.defense = fighter.defense
    this.health = fighter.health
  }

  getHitPower() {
    const critChance = +(Math.random() + 1).toFixed(2)
    return this.attack * critChance
  }

  getBlockPower() {
      const dodgeChance = +(Math.random() + 1).toFixed(2)
      return this.defense * dodgeChance
    }
}

export default Fighter
