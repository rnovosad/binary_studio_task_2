import View from "./view";

class ModalView extends View {
  constructor(fighter, updateFighter) {
    super()

    this.fighter = fighter
    this.updateFighter = updateFighter
    this.handleClose = this.handleClose.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.createModal(this.fighter)
  }

  handleClose() {
    const close = document.querySelector('[id^="fighterDetails"]')
    close.parentNode.removeChild(close)
  }

  handleSave() {
    const form = document.getElementsByTagName('input')
    const upd = Array.from(form).map(el => {
      return {
        [el.name]: el.value
      }
    })
    this.handleClose()
    return Object.assign({}, ...upd)
  }

  createModal(fighter) {
    const existingModal = document.querySelector('[id^="fighterDetails"]')
    if (existingModal) {
      return this.element = existingModal
    }
    this.element = this.createElement({ tagName: 'div', className: 'modal', attributes: {id: `fighterDetails-${fighter.name}`} });
    this.element.append(this.createForm(fighter))
    document.getElementById('root').append(this.element)
  }

  createForm(fighter) {
    const form = this.createElement({ tagName: 'div', className: 'modal-container' })
    const label = this.createElement({ tagName: 'h1', className: 'name'})
    label.innerText = fighter.name
    form.append(this.createContainer(label, this.closeButton()))
    for (let k in fighter) {
      form.append(this.createContainer(this.createLabel(k), this.createInput(k, fighter[k])))
    }
    form.append(this.saveButton())
    return form
  }

  createContainer(...el) {
    const container = this.createElement({ tagName: 'div', className: 'empty' })
    container.append(...el)
    return container
  }

  closeButton() {
    const closeBtn = this.createElement({
      tagName: 'span',
      className: 'btn'
    })
    closeBtn.innerText = 'X'
    closeBtn.onclick = () => this.handleClose()
    return closeBtn;
  }

  saveButton() {
    const saveBtn = this.createElement({
      tagName: 'button',
      className: 'btn-save'
    })
    saveBtn.innerText = 'Save'
    saveBtn.onclick = () => this.updateFighter(this.handleSave())
    return saveBtn;
  }

  createLabel(name) {
    const label = this.createElement({
      tagName: 'label',
      className: 'label',
      attributes: { name }
    })
    label.innerText = name
    return label
  }

  createInput(name, data) {
    const block = this.createElement({
      tagName: 'input',
      className: 'input',
      attributes: { name }
    })
    block.value = data
    return block
  }
}

export default ModalView;
