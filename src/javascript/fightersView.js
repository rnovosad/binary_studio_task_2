import View from './view';
import FighterView from './fighterView';
import { fighterService } from "./services/fightersService";
import ModalView from "./modalView";
import Fighter from "./models/fighter";
import Battle from "./services/battleService";

class FightersView extends View {
  constructor(fighters) {
    super();
    this.updateFighter = this.updateFighter.bind(this)
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });
    const i1 = this.createInput('fighter1')
    const i2 = this.createInput('fighter2')
    const fb = this.fightButton()

    const f = this.createElement({ tagName: 'div', className: 'fighters' });
    f.append(...fighterElements);
    this.element = this.createContainer(f, i1, i2, fb)
  }

  createContainer(...el) {
    const container = this.createElement({ tagName: 'div', className: 'box', attributes: {id: 'box'} })
    container.append(...el)
    return container
  }

  updateFighter(fighter) {
    this.fightersDetailsMap.set(fighter._id, fighter)
  }

  showModal(fighter) {
    return new ModalView(fighter, this.updateFighter)
  }

  createInput(name) {
    return this.createElement({
      tagName: 'input',
      className: 'input',
      attributes: { id: name, placeholder: 'Enter id' }
    })
  }

  createResult(name) {
    const box = document.getElementById('box')
    const result = this.createElement({
      tagName: 'div',
      className: 'name',
      attributes: {
        id: 'result'
      }
    })
    result.innerText = `And the winner is ${name}`
    let oldChild = document.getElementById('result')
    if (oldChild) {
      box.replaceChild(result, oldChild)
      return box
    }
    box.append(result)
    return box
  }

  fightButton() {
    const fightBtn = this.createElement({
      tagName: 'button',
      className: 'btn-save'
    })
    fightBtn.innerText = 'Fight'
    fightBtn.onclick = () => this.fight()
    return fightBtn;
  }

  fight() {
    const f1 = this.fightersDetailsMap.get(document.getElementById('fighter1').value)
    const f2 = this.fightersDetailsMap.get(document.getElementById('fighter2').value)
    const fighter1 = new Fighter(f1)
    const fighter2 = new Fighter(f2)
    const battle = new Battle(fighter1, fighter2)
    return this.createResult(battle.fight())
  }

  handleFighterClick(event, fighter) {
    if (!this.fightersDetailsMap.get(fighter._id)) {
      fighterService.getFighterDetails(fighter._id)
        .then(f => {
          this.fightersDetailsMap.set(fighter._id, f)
          this.showModal(f)
        })
    } else {
      this.showModal(this.fightersDetailsMap.get(fighter._id))
    }
  }
}

export default FightersView;
